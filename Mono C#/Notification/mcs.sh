#!/bin/bash

#mcs -r:notify-sharp.dll,dbus-sharp.dll,dbus-sharp-glib.dll \
    #-out:run.exe \
    #notificationExample.cs

mcs -pkg:dbus-sharp-2.0,dbus-sharp-glib-2.0,notify-sharp \
    -out:run.exe \
    notificationExample.cs

