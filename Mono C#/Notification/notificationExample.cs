using System;
using System.IO;

using Notifications;

namespace example {
public class NotificationExample {

    public static void Main() {
        Console.Write ("Testing...\n");
        
        var notify = init();
        notify.Show();

    } //end ctor

    public static Notification init() {
        var Hello = new Notification();
        Hello.Summary = "Summary Hello";
        //Hello.Body = "This is an example notification";
        Hello.Body = "もう3時じゃん！こうなったらオールナイト覚悟だねぇ";
        Hello.IconName = "dialog-information";
        //Hello.IconName = "weather-snow";

        return Hello;
    }


} //end class NotificationExample
} //end namespace
