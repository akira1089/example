using System;
using System.Threading;

namespace example {
public class MultiThreadExample {
    public static void Main (string[] argv) {

        ThreadStart job = new ThreadStart (ThreadJob);
        Thread t = new Thread (job);
        t.Start();

        for (int i=0; i < 5; i++) {
            Console.Write ("Main thread: {0}\n", i);
            Thread.Sleep (1000);
        }
    }

    private static void ThreadJob() {
        for (int i=0; i < 10; i++) {
            Console.Write ("\tOther thread: {0}\n", i);
            Thread.Sleep (500);
        }
    }
} //end class MultiThreadExample
} //end namespace example
