using System;
using System.IO;

using System.Text;

namespace example {

public class GetBytesExample {

    public static void Main() {

        string test = "もう1時だねー、金曜じゃなくてもカレーだよね！艦娘の数だけ、カレーがあるんだ！";

        int getUTF8Bytes = Encoding.UTF8.GetByteCount (test);
        int getASCIIBytes = Encoding.ASCII.GetByteCount (test);

        Console.Write ("String length:\t{0}\nUTF8 Byte Length:\t{1}\nASCII Byte Length:\t{2}\n",
            test.Length, getUTF8Bytes, getASCIIBytes);

    }


} //end class
} //end namespace
