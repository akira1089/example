using System;
using System.IO;

namespace test {
public class TestString {

    private static void Main (string[] argv) {

        string original =
        "/mnt/Ak30-OSISO/ipod/0 - [Work]/Vocal/00. Singles/04 Light colors.flac";

        Console.Write ("Original string: {0}\n", original);

        Uri uri = new Uri (original);
        
        Console.Write ("Uri.AbsoluteUri: {0}\n" +
            "Uri.LocalPath: {1}\n",
            uri.AbsoluteUri, uri.LocalPath);

        string delimit = delimiter (original);
        Console.Write ("Delimit Replace: {0}\n", delimit);

        string fileUri = "file://" + @delimit;
        Console.Write ("FileURI: {0}\n", fileUri);

    }

    private static string delimiter (string str) {

        string replaced = str.Replace (" ", @"\ ").Replace 
            ("(", @"\(").Replace (")", @"\)").Replace
            ("[", @"\[").Replace ("]", @"\]").Replace 
            ("{", @"\{").Replace ("}", @"\}");

        return @replaced;
    }
} //end class TestString
} //end namespace test
