import javafx.application.Application;
import javafx.stage.Stage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import javafx.util.Duration;

import java.io.File;

public class HelloFX extends Application {

    private static MediaPlayer mediaPlayer;
    private static Media media;

    public static void main (String[] args) {
        launch (args);
    } //end static void main

    @Override
    public void start (Stage primaryStage) {
        primaryStage.setTitle ("Hello World FX!");

        String mediaFilePath = new File ("src/unohana_error.wav").
            toURI().toString();

        media = new Media (mediaFilePath);
        mediaPlayer = new MediaPlayer (media);

        Button btn = new Button();
        btn.setText ("Say 'Hello World'");
        btn.setOnAction (new EventHandler<ActionEvent>() {
            
            @Override
            public void handle (ActionEvent event) {
                System.out.println ("Hello World FX");
                mediaPlayer.stop();
                mediaPlayer.setStartTime (Duration.ZERO);
                mediaPlayer.play();
            }

        });

        StackPane root = new StackPane();
        root.getChildren().add (btn);
        primaryStage.setScene (new Scene (root, 300, 250));

        primaryStage.show();

    }

} //end class HelloFX
