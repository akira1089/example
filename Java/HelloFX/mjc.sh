#/bin/bash
# mjc.sh
# javac with lwjgl

LWJGL_DIR="lwjgl-3.1.1"
LWJGL_CP="${LWJGL_DIR}/lwjgl.jar:"
LWJGL_CP+="${LWJGL_DIR}/lwjgl-glfw.jar:"
LWJGL_CP+="${LWJGL_DIR}/lwjgl-opengl.jar:"
LWJGL_CP+="${LWJGL_DIR}/lwjgl-openal.jar"

#javac -cp "${LWJGL_CP}:." \
javac \
    *.java
