#!/usr/bin/ruby

require 'notify-send'
require 'timers'

module Example

timers = Timers::Group.new

ten_sec_timer = timers.after (10) { 
    NotifySend.send "After 10 seconds", 
        "10 has passed and will wait for 5 seconds!",
        "info", 5000
}

NotifySend.send "Title is a very long long long title", 
    "extra text of summarization", "info", 10000

timers.wait

end
